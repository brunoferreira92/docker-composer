FROM johanvanhelden/dockerhero-php-5.6-fpm

MAINTAINER Gustavo Bacelar <gubacelar@gmail.com>

# Install mysql
RUN apt-get update && apt-get -y install \
    mysql-client
#
COPY ioncube_loader_lin_5.6.so /usr/local/lib/php/extensions/no-debug-non-zts-20131226/ioncube_loader_lin_5.6.so
